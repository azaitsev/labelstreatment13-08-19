program LabelsTreatment
use var
implicit none
 integer k
 call LoadVar
 allocate (T(Npoints,NTimeSteps),x(Npoints),y(Npoints),xmax(NTimeSteps),vmax(NTimeSteps),Tmax(Npoints),z(Npoints),xmax2(NTimeSteps),xmax3(NTimeSteps))
 do k=1,NumberOfFiles
  call LoadData(k)
  call treatdata !(k)
  call savedata(k)
 enddo
end  program LabelsTreatment