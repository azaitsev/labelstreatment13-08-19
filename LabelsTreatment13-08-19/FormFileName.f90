!call FormFileName(filename1,k,StartStep)
 subroutine FormFileName(filename1,k,StartStep)
  use var
  implicit none
  character(50) filename1
  integer k,StartStep
  character(50) s1,s2
  filename1=""; s1=""; s2="";
  if(AddZero==0) then
    write(s1,'(i)') StartStep
   else 
    write(s2,'(a2,i1,a1,i1,a1)') '(i',AddZero,'.',AddZero,')'
    write(s1,s2) StartStep
   endif
   write(filename1,*) trim(filename(k)),trim(ADJUSTL(s1)),".",trim(ADJUSTL(filenameextension))
 end subroutine FormFileName