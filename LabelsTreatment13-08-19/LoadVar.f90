subroutine LoadVar
use var
implicit none
 character(50) var_name
 real var_val
 integer k
 character(50) filename1
 
 open(41,file="init.var")

 DO WHILE (.NOT. EOF(41))
  read(41,*) var_name,var_val
  call UpperCase(len(var_name),var_name)
 
 SELECT CASE (trim(var_name))

  CASE ('NPOINTS')
  Npoints=var_val
  print *,var_name,var_val

  CASE ('NTIMESTEPS')
  NTimeSteps=var_val
  print *,var_name,var_val

  CASE ('STEPDELTA')
  StepDelta=var_val
  print *,var_name,var_val

  CASE ('STARTTIME')
  StartTime=var_val
  print *,var_name,var_val

  CASE ('DIM3DOR2D')
  Dim3Dor2D=var_val
  print *,var_name,var_val

  CASE ('ADDZERO')
  AddZero=var_val
  print *,var_name,var_val

  case ('FILENAMEEXTENSION')
  read(41,'(a6)') filenameextension
  print *,var_name,filenameextension

  case ('XENDHEATER,M')
  xendheater=var_val
  print *,var_name,var_val
  
  case ('NUMBEROFFILES')
  NumberOfFiles=var_val
  allocate (filename(NumberOfFiles))
  print *,var_name,var_val
  do k=1,NumberOfFiles
   read(41,*) filename(k)
   call FormFileName(filename1,k,StartTime)
   print *,filename1
  enddo

 END SELECT

 enddo
 close(41)

end  subroutine LoadVar



 subroutine UpperCase(N,varname)
  implicit none
  integer N
  character varname(N)
  where (varname>='a'.and.varname<='z') varname=char(ichar(varname)+ichar('A')-ichar('a'))
 ! varname
  !end where
  !do i=1,N
  ! 
  !enddo
 end subroutine UpperCase