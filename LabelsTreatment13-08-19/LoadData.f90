subroutine LoadData(k)
use var
implicit none
 integer k
 character(50) filename1,s1
 integer Step,i,n1
 do Step=1,NTimeSteps
  call FormFileName(filename1,k,StartTime+(Step-1)*StepDelta)
  open(41,file=filename1)
  read(41,*) s1 ,s1,s1
  do i=1,Npoints
   if(Dim3Dor2D==2) then
    read(41,*) n1,x(i),y(i),T(i,step)
   else
    read(41,*) n1,x(i),y(i),z(i),T(i,step)
   endif
  enddo !i=1,Npoints
  close(41)
 enddo !do Step=1,NTimeSteps 
end subroutine LoadData