subroutine savedata(k)
use var
implicit none
 integer k
 character(40) filename1
  integer Step,i
 write(filename1,*) trim(filename(k))//"temp.dat"
 open(41,file=filename1)
 print *,"Saving ",filename1
 if(dim3Dor2D==2) then
  write(41,*) 'VARIABLES="X","Y","time","T"'
 else
  write(41,*) 'VARIABLES="X","Y","Z","time","T"'
 endif
 write(41,*)'ZONE  T=',trim(filename(k)),',I=',Npoints,',J=',NTimeSteps
 do Step=1,NTimeSteps
   do i=1,Npoints
    if(dim3Dor2D==2) then
     write(41,*) x(i),y(i),StartTime+(Step-1)*StepDelta,T(i,step)
   else
     write(41,*) x(i),y(i),z(i),StartTime+(Step-1)*StepDelta,T(i,step)
   endif
  enddo
 enddo
 close(41)

  write(filename1,*) trim(filename(k))//"xmax.dat"
 open(41,file=filename1)
 print *,"Saving ",filename1
 write(41,*) 'VARIABLES="time","Xmax","Vmax","Tmax","Xmax2","Xmax3"'
 write(41,*)'ZONE  T=',trim(filename(k)),',I=',NTimeSteps
 do Step=1,NTimeSteps
   write(41,*) StartTime+(Step-1)*StepDelta,xmax(step),vmax(step),Tmax(step),xmax2(Step),xmax3(Step)
 enddo
 close(41) 

end  subroutine savedata