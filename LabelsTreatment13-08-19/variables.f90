module var
 integer::NTimeSteps=100, Npoints=501,StepDelta=10,StartTime=10,NumberOfFiles=1
 integer::Dim3Dor2D=3,AddZero=0
 real::xEndHeater=2.0
 real,allocatable::T(:,:),x(:),y(:),xmax(:),vmax(:),Tmax(:),z(:),xmax2(:),xmax3(:)
 character(50),allocatable::filename(:)
 character(10)::filenameextension=''
end module var